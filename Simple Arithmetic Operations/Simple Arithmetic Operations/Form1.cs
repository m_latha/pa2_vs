﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_Arithmetic_Operations
{
    public partial class Form1 : Form
    {

        int FirstNumber, SecondNumber,Sum;
        int substract;
        int multiply;
        int divide;

        private void btn_Quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Application.Exit();
            Application.Exit();
        }

        private void btn_subtract_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_firstnumber.Text);
            SecondNumber = int.Parse(txt_secondnumber.Text);
            Sum = FirstNumber - SecondNumber;
            MessageBox.Show(" The differnce between the two numbers is " + Sum.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_firstnumber.Text);
            SecondNumber = int.Parse(txt_secondnumber.Text);
            multiply = FirstNumber * SecondNumber;
            MessageBox.Show(" The multiplication of the two numbers is " + multiply.ToString());
        }

        private void btn_divide_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_firstnumber.Text);
            SecondNumber = int.Parse(txt_secondnumber.Text);
            divide = FirstNumber / SecondNumber;
            MessageBox.Show(" The division of two numbers is " + divide.ToString());
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_firstnumber.Text);
            SecondNumber = int.Parse(txt_secondnumber.Text);
            Sum = FirstNumber + SecondNumber;
            MessageBox.Show(" The sum of the two numbers is " + Sum.ToString());
        }
    }
}
